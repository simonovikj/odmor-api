export enum UserStatus {
  Deactivated = 'deactivated',
  Pending = 'pending',
  Active = 'active',
  Inactive = 'inactive',
}
