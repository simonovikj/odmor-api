"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.BookingStatus = void 0;
var BookingStatus;
(function (BookingStatus) {
    BookingStatus["Pending"] = "Pending";
    BookingStatus["Approved"] = "Approved";
    BookingStatus["Rejected"] = "Rejected";
})(BookingStatus = exports.BookingStatus || (exports.BookingStatus = {}));
//# sourceMappingURL=booking-status.enum.js.map