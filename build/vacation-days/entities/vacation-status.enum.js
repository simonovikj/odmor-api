"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.VacationStatus = void 0;
var VacationStatus;
(function (VacationStatus) {
    VacationStatus["Pending"] = "Pending";
    VacationStatus["Approved"] = "Approved";
    VacationStatus["Rejected"] = "Rejected";
})(VacationStatus = exports.VacationStatus || (exports.VacationStatus = {}));
//# sourceMappingURL=vacation-status.enum.js.map